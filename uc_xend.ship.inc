<?php

/**
 * @file
 * Xend Web Services Rate / Available Services Quote.
 *
 * Shipping quote module that interfaces with the Xend Web Services API
 * to get rates for small package shipments.  Implements a SOAP Web Service
 * client.
 *
 * @author dzieyzone.    <http://drupal.org/user/269019>
 */


/**
 * Shipment creation callback.
 *
 * Confirms shipment data before requesting a shipping label.
 *
 * @param $order_id
 *   The order id for the shipment.
 * @param $package_ids
 *   Array of package ids to shipped.
 *
 * @see uc_xend_fulfill_order_validate()
 * @see uc_xend_fulfill_order_submit()
 * @ingroup forms
 */
function uc_xend_fulfill_order($form, &$form_state, $order, $package_ids) {
  $pkg_types = _uc_xend_pkg_types();
  $form['order_id'] = array('#type' => 'value', '#value' => $order->order_id);
  $packages = array();
  $addresses = array();

  // Container for package data
  $form['packages'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Packages'),
    '#collapsible' => TRUE,
    '#tree'        => TRUE
  );
  foreach ($package_ids as $id) {
    $package = uc_shipping_package_load($id);
	  drupal_set_message(print_r($package, true));
    if ($package) {
      foreach ($package->addresses as $address) {
        if (!in_array($address, $addresses)) {
          $addresses[] = $address;
        }
      }
      // Create list of products and get a representative product (last one in
      // the loop) to use for some default values
      $product_list = array();
      $declared_value = 0;
      foreach ($package->products as $product) {
        $product_list[] = $product->qty . ' x ' . $product->model;
        $declared_value += $product->qty * $product->price;
      }
      // Use last product in package to determine package type
      $xend_data = db_query("SELECT pkg_type FROM {uc_xend_products} WHERE nid = :nid", array(':nid' => $product->nid))->fetchAssoc();
      $product->xend = $xend_data;
      $pkg_form = array(
        '#type' => 'fieldset',
        '#title' => t('Package !id', array('!id' => $id)),
      );
      $pkg_form['products'] = array(
        '#theme' => 'item_list',
        '#items' => $product_list,
      );
      $pkg_form['package_id'] = array(
        '#type'  => 'hidden',
        '#value' => $id,
      );
      $pkg_form['pkg_type'] = array(
        '#type'          => 'select',
        '#title'         => t('Package type'),
        '#options'       => $pkg_types,
        '#default_value' => $product->xend['pkg_type'],
        '#required'      => TRUE,
      );
      $pkg_form['declared_value'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Declared value'),
        '#default_value' => $declared_value,
        '#required'      => TRUE,
      );
      $pkg_form['weight'] = array(
        '#type'           => 'container',
        '#attributes'     => array('class' => array('uc-inline-form', 'clearfix')),
        '#description'    => t('Weight of the package. Default value is sum of product weights in the package.'),
        '#weight'         => 15,
      );
      $pkg_form['weight']['weight'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Weight'),
        '#default_value' => isset($package->weight) ? $package->weight : 0,
        '#size'          => 10,
        '#maxlength'     => 15,
      );
      $pkg_form['weight']['units'] = array(
        '#type'          => 'select',
        '#title'         => t('Units'),
        '#options'       => array(
          'kg' => t('Kilograms'),
        ),
        '#default_value' => isset($package->weight_units) ?
                            $package->weight_units        :
                            variable_get('uc_weight_unit', 'kg'),
      );
      $pkg_form['dimensions'] = array(
        '#type'          => 'container',
        '#attributes'    => array('class' => array('uc-inline-form', 'clearfix')),
        '#description'   => t('Physical dimensions of the package.'),
        '#weight'        => 20,
      );
      $pkg_form['dimensions']['length'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Length'),
        '#default_value' => isset($package->length) ? $package->length : 1,
        '#size'          => 10,
      );
      $pkg_form['dimensions']['width'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Width'),
        '#default_value' => isset($package->width) ? $package->width : 1,
        '#size'          => 10,
      );
      $pkg_form['dimensions']['height'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Height'),
        '#default_value' => isset($package->height) ? $package->height : 1,
        '#size'          => 10,
      );
      $pkg_form['dimensions']['units'] = array(
        '#type'          => 'select',
        '#title'         => t('Units'),
        '#options'       => array(
          'in' => t('Inches'),
          'ft' => t('Feet'),
          'cm' => t('Centimeters'),
          'mm' => t('Millimeters'),
        ),
        '#default_value' => isset($package->length_units) ?
                            $package->length_units        :
                            variable_get('uc_length_unit', 'cm'),
      );

      $form['packages'][$id] = $pkg_form;
    }
  }

  $form = uc_shipping_address_form($form, $form_state, $addresses, $order);

  foreach (array('delivery_email', 'delivery_last_name', 'delivery_street1', 'delivery_city', 'delivery_zone', 'delivery_country', 'delivery_postal_code') as $field) {
    $form['destination'][$field]['#required'] = TRUE;
  }

  // Determine shipping option chosen by the customer
  $method  = $order->quote['method'];
  $methods = module_invoke_all('uc_shipping_method');
  if (isset($methods[$method])) {
    $services = $methods[$method]['quote']['accessorials'];
    $method   = $services[$order->quote['accessorials']];
  }

  // Container for shipment data
  $form['shipment'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Shipment data'),
    '#collapsible' => TRUE,
  );

  // Inform user of customer's shipping choice
  $form['shipment']['shipping_choice'] = array(
    '#type'   => 'markup',
    '#prefix' => '<div>',
    '#markup' => t('Customer selected "@method" as the shipping method and paid @rate', array('@method' => $method, '@rate' => uc_currency_format($order->quote['rate']))),
    '#suffix' => '</div>',
  );

  // Pass shipping charge paid information on to validation function so it
  // can be displayed alongside actual costs
  $form['shipment']['paid'] = array(
    '#type'  => 'value',
    '#value' => uc_currency_format($order->quote['rate']),
  );

  $services = _uc_xend_pkg_types(); //_uc_xend_service_list();
  $default_service = '';
  if ($method == 'xend') {
    $default_service = $order->quote['accessorials'];
  }

  $form['shipment']['service'] = array(
    '#type'          => 'select',
    '#title'         => t('Xend service'),
    '#options'       => $services,
    '#default_value' => $default_service,
  );
  $today = getdate();
  $form['shipment']['ship_date'] = array(
    '#type'          => 'date',
    '#title'         => t('Ship date'),
    '#default_value' => array(
      'year'  => $today['year'],
      'month' => $today['mon'],
      'day'   => $today['mday']
    ),
  );
  $form['shipment']['expected_delivery'] = array(
    '#type'          => 'date',
    '#title'         => t('Expected delivery'),
    '#default_value' => array(
      'year'  => $today['year'],
      'month' => $today['mon'],
      'day'   => $today['mday']
    ),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Review shipment')
  );

  return $form;
}

/**
 * Passes final information into shipment object.
 *
 * @see uc_xend_fulfill_order()
 * @see uc_xend_confirm_shipment()
 */
function uc_xend_fulfill_order_validate($form, &$form_state) {

  $errors = form_get_errors();
  if (isset($errors)) {
    // Some required elements are missing - don't bother with making
    // a Xend API call until that gets fixed.
    return;
  }

  $origin = new stdClass();
  $destination = new stdClass();
  $packages = array();
  foreach ($form_state['values'] as $key => $value) {
    if (substr($key, 0, 7) == 'pickup_') {
      $field = substr($key, 7);
      $origin->$field = $value;
    }
    elseif (substr($key, 0, 9) == 'delivery_') {
      $field = substr($key, 9);
      $destination->$field = $value;
    }
  }

  // This is a total hack to work around changes made in the return value
  // from uc_shipping_address_form().  That function needs to be fixed, but
  // until then this should do the trick.
  $origin        = $form_state['values']['pickup_address'];
  $origin->phone = $form_state['values']['phone'];
  $origin->first_name = $form_state['values']['first_name'];
  $origin->last_name = $form_state['values']['last_name'];
  $origin->company = $form_state['values']['company'];
  $origin->street1 = $form_state['values']['street1'];
  $origin->street2 = $form_state['values']['street2'];
  $origin->city = $form_state['values']['city'];
  $origin->zone = $form_state['values']['zone'];
  $origin->country = $form_state['values']['country'];
  $origin->postal_code = $form_state['values']['postal_code'];
  $origin->email = $form_state['values']['pickup_email'];

  $_SESSION['xend'] = array();
  $_SESSION['xend']['origin'] = $origin;

  if (empty($destination->company)) {
    $destination->company = $destination->first_name . ' ' . $destination->last_name;
  }

  $_SESSION['xend']['destination'] = $destination;
  foreach ($form_state['values']['packages'] as $id => $pkg_form) {
    $package               = uc_shipping_package_load($id);
    $package->pkg_type     = $pkg_form['pkg_type'];
    $package->value        = $pkg_form['declared_value'];
    $package->weight       = $pkg_form['weight']['weight'];
    $package->weight_units = $pkg_form['weight']['units'];
    $package->length       = $pkg_form['dimensions']['length'];
    $package->width        = $pkg_form['dimensions']['width'];
    $package->height       = $pkg_form['dimensions']['height'];
    $package->length_units = $pkg_form['dimensions']['units'];
    $package->qty          = 1;
    $_SESSION['xend']['packages'][$id] = $package;
  }
  $_SESSION['xend']['service']           = $form_state['values']['service'];
  $_SESSION['xend']['paid']              = $form_state['values']['paid'];
  $_SESSION['xend']['ship_date']         = $form_state['values']['ship_date'];
  $_SESSION['xend']['expected_delivery'] = $form_state['values']['expected_delivery'];
  $_SESSION['xend']['order_id'] = $form_state['values']['order_id'];

  $request = uc_xend_shipment_request($_SESSION['xend']['packages'], $origin, $destination, $form_state['values']['service']);
  $response_obj = drupal_http_request('https://wwwcie.xend.com/xend.app/xml/ShipConfirm', array(
    'method' => 'POST',
    'data' => $request,
  ));
  $response = new SimpleXMLElement($response_obj->data);
  if (isset($response->Response->Error)) {
    $error = $response->Response->Error;
    $error_msg = (string) $error->ErrorSeverity . ' Error ' . (string) $error->ErrorCode . ': ' . (string) $error->ErrorDescription;
    if (strpos((string) $error->ErrorSeverity, 'Hard') !== FALSE) {
      form_set_error('', $error_msg);
      return FALSE;
    }
    else {
      drupal_set_message($error_msg, 'error');
    }
  }
  $charge = new stdClass();
  // if NegotiatedRates exist, quote based on those, otherwise, use TotalCharges
  if (isset($response->ShipmentCharges)) {
    $charge = $response->ShipmentCharges->TotalCharges;
    $_SESSION['xend']['rate']['type'] = t('Total Charges');
    if (isset($response->NegotiatedRates)) {
      $charge = $response->NegotiatedRates->NetSummaryCharges->GrandTotal;
      $_SESSION['xend']['rate']['type'] = t('Negotiated Rates');
    }
  }
  $_SESSION['xend']['rate']['currency'] = (string) $charge->CurrencyCode;
  $_SESSION['xend']['rate']['amount'] = (string) $charge->MonetaryValue;
  $_SESSION['xend']['digest'] = (string) $response->ShipmentDigest;
}

/**
 * Passes final information into shipment object.
 *
 * @see uc_xend_fulfill_order()
 * @see uc_xend_confirm_shipment()
 */
function uc_xend_fulfill_order_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/store/orders/' . $form_state['values']['order_id'] . '/shipments/xend';
}

/**
 * Constructs an XML shipment request.
 *
 * @param $packages
 *   Array of packages received from the cart.
 * @param $origin
 *   Delivery origin address information.
 * @param $destination
 *   Delivery destination address information.
 * @param $xend_service
 *   Xend service code (refers to Xend Ground, Next-Day Air, etc.).
 *
 * @return
 *   ShipConfirm XML document to send to Xend.
 */
function uc_xend_shipment_request($packages, $origin, $destination, $xend_service) {
  $store['name'] = uc_store_name();
  $store['owner'] = variable_get('uc_store_owner', NULL);
  $store['email'] = uc_store_email();
  $store['email_from'] = uc_store_email();
  $store['phone'] = variable_get('uc_store_phone', NULL);
  $store['fax'] = variable_get('uc_store_fax', NULL);
  $store['street1'] = variable_get('uc_store_street1', NULL);
  $store['street2'] = variable_get('uc_store_street2', NULL);
  $store['city'] = variable_get('uc_store_city', NULL);
  $store['zone'] = variable_get('uc_store_zone', NULL);
  $store['postal_code'] = variable_get('uc_store_postal_code', NULL);
  $store['country'] = variable_get('uc_store_country', 840);

  $ua = explode(' ', $_SERVER['HTTP_USER_AGENT']);
  $user_agent = $ua[0];

  $services = _uc_xend_pkg_types;//_uc_xend_service_list();
  $service = array('code' => $xend_service, 'description' => $services[$xend_service]);

  $pkg_types = _uc_xend_pkg_types();

  $shipper_zone = uc_get_zone_code($store['zone']);
  $shipper_country = uc_get_country_data(array('country_id' => $store['country']));
  $shipper_country = $shipper_country[0]['country_iso_code_2'];
  $shipper_zip = $store['postal_code'];
  $shipto_zone = uc_get_zone_code($destination->zone);
  $shipto_country = uc_get_country_data(array('country_id' => $destination->country));
  $shipto_country = $shipto_country[0]['country_iso_code_2'];
  $shipto_zip = $destination->postal_code;
  $shipfrom_zone = uc_get_zone_code($origin->zone);
  $shipfrom_country = uc_get_country_data(array('country_id' => $origin->country));
  $shipfrom_country = $shipfrom_country[0]['country_iso_code_2'];
  $shipfrom_zip = $origin->postal_code;

  $xend_units = variable_get('uc_xend_unit_system', variable_get('uc_length_unit', 'in'));

  $package_schema = '';

  $client = uc_xend_access_request();

  return $schema;
}

/**
 * Last chance for user to review shipment.
 *
 * @see uc_xend_confirm_shipment_submit()
 * @see theme_uc_xend_confirm_shipment()
 * @ingroup forms
 */
function uc_xend_confirm_shipment($form, &$form_state, $order) {
  $form['digest'] = array(
    '#type' => 'hidden',
    '#value' => $_SESSION['xend']['digest']
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Request Pickup')
  );

  return $form;
}

/**
 * Displays final shipment information for review.
 *
 * @see uc_xend_confirm_shipment()
 * @ingroup themeable
 */
function theme_uc_xend_confirm_shipment($variables) {
  $form = $variables['form'];

  $output = '<div class="shipping-address"><b>' . t('Ship from:') . '</b><br />';
  $output .= uc_address_format(
    check_plain($_SESSION['xend']['origin']->first_name),
    check_plain($_SESSION['xend']['origin']->last_name),
    check_plain($_SESSION['xend']['origin']->company),
    check_plain($_SESSION['xend']['origin']->street1),
    check_plain($_SESSION['xend']['origin']->street2),
    check_plain($_SESSION['xend']['origin']->city),
    check_plain($_SESSION['xend']['origin']->zone),
    check_plain($_SESSION['xend']['origin']->postal_code),
    check_plain($_SESSION['xend']['origin']->country)
  );
  $output .= '<br />' . check_plain($_SESSION['xend']['origin']->email);
  $output .= '</div>';

  $output .= '<div class="shipping-address"><b>' . t('Ship to:') . '</b><br />';
  $output .= uc_address_format(
    check_plain($_SESSION['xend']['destination']->first_name),
    check_plain($_SESSION['xend']['destination']->last_name),
    check_plain($_SESSION['xend']['destination']->company),
    check_plain($_SESSION['xend']['destination']->street1),
    check_plain($_SESSION['xend']['destination']->street2),
    check_plain($_SESSION['xend']['destination']->city),
    check_plain($_SESSION['xend']['destination']->zone),
    check_plain($_SESSION['xend']['destination']->postal_code),
    check_plain($_SESSION['xend']['destination']->country)
  );
  $output .= '<br />' . check_plain($_SESSION['xend']['destination']->email);
  $output .= '</div>';
  $output .= '<div class="shipment-data">';
  $method = uc_xend_uc_shipping_method();
  $output .= '<b>' . $method['xend']['quote']['accessorials'][$_SESSION['xend']['service']] . '</b><br />';
  $output .= '<i>' . check_plain($_SESSION['xend']['rate']['type']) . '</i>: ' . theme('uc_price', array('price' => $_SESSION['xend']['rate']['amount'])) . ' (' . check_plain($_SESSION['xend']['rate']['currency']) . ') -- ';
  $output .= '<i>' . t('Paid') . '</i>: ' . $_SESSION['xend']['paid'] . '<br />';
  $ship_date = $_SESSION['xend']['ship_date'];
  $output .= 'Ship date: ' . format_date(gmmktime(12, 0, 0, $ship_date['month'], $ship_date['day'], $ship_date['year']), 'uc_store');
  $exp_delivery = $_SESSION['xend']['expected_delivery'];
  $output .= '<br />Expected delivery: ' . format_date(gmmktime(12, 0, 0, $exp_delivery['month'], $exp_delivery['day'], $exp_delivery['year']), 'uc_store');
  $output .= "</div>\n<br style=\"clear: both;\" />";
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Generates label and schedules pickup of the shipment.
 *
 * @see uc_xend_confirm_shipment()
 */
function uc_xend_confirm_shipment_submit($form, &$form_state) {
  // Request pickup using parameters in form.
  $order_id = $_SESSION['xend']['order_id'];
  $packages = array_keys($_SESSION['xend']['packages']);
  $request = uc_xend_request_pickup($form_state['values']['digest'], $order_id, $packages);
  $result = drupal_http_request('https://wwwcie.xend.com/xend.app/xml/ShipAccept', array(
    'method' => 'POST',
    'data' => $request,
  ));
  $response = new SimpleXMLElement($result->data);
  $code = (string) $response->Response->ResponseStatusCode;
  if ($code == 0) { // failed request
    $error = $response->Response->Error;
    $error_severity = (string) $error->ErrorSeverity;
    $error_code = (string) $error->ErrorCode;
    $error_description = (string) $error->ErrorDescription;
    drupal_set_message(t('(@severity error @code) @description', array('@severity' => $error_severity, '@code' => $error_code, '@description' => $error_description)), 'error');
    if ($error_severity == 'HardError') {
      $form_state['redirect'] = 'admin/store/orders/' . $order_id . '/shipments/xend/' . implode('/', $packages);
      return;
    }
  }

  $shipment = new stdClass();
  $shipment->order_id = $order_id;
  $shipment->origin = clone $_SESSION['xend']['origin'];
  $shipment->destination = clone $_SESSION['xend']['destination'];
  $shipment->packages = $_SESSION['xend']['packages'];
  $shipment->shipping_method = 'xend';
  $shipment->accessorials = $_SESSION['xend']['service'];
  $shipment->carrier = t('Xend');
  // if NegotiatedRates exist, quote based on those, otherwise, use TotalCharges
  if (isset($response->ShipmentResults->ShipmentCharges)) {
    $charge = $response->ShipmentResults->ShipmentCharges->TotalCharges;
    if (isset($response->ShipmentResults->NegotiatedRates)) {
      $charge = $response->ShipmentResults->NegotiatedRates->NetSummaryCharges->GrandTotal;
    }
  }
  $cost = (string) $charge->MonetaryValue;
  $shipment->cost = $cost;
  $shipment->tracking_number = (string) $response->ShipmentResults->ShipmentIdentificationNumber;
  $ship_date = $_SESSION['xend']['ship_date'];
  $shipment->ship_date = gmmktime(12, 0, 0, $ship_date['month'], $ship_date['day'], $ship_date['year']);
  $exp_delivery = $_SESSION['xend']['expected_delivery'];
  $shipment->expected_delivery = gmmktime(12, 0, 0, $exp_delivery['month'], $exp_delivery['day'], $exp_delivery['year']);

  foreach ($response->ShipmentResults->PackageResults as $package_results) {
    $package =& current($shipment->packages);
    $package->tracking_number = (string) $package_results->TrackingNumber;
    //$label_image = (string) $package_results->LabelImage->GraphicImage;
    $label_image =  '';
    // Save the label
    $directory = 'public://xend_labels';
    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      $label_path = $directory . '/label-' . $package->tracking_number . '.gif';
      if ($label_file = file_save_data(base64_decode($label_image), $label_path, FILE_EXISTS_REPLACE)) {
        file_usage_add($label_file, 'uc_shipping', 'package', $package->package_id);
        $package->label_image = $label_file;
      }
      else {
        drupal_set_message(t('Could not open a file to save the label image.'), 'error');
      }
    }
    else {
      drupal_set_message(t('Could not find or create the directory %directory in the file system path.', array('%directory' => $directory)), 'error');
    }
    unset($package);
    next($shipment->packages);
  }

  uc_shipping_shipment_save($shipment);

  unset($_SESSION['xend']);
  $form_state['redirect'] = 'admin/store/orders/' . $order_id . '/shipments';
}

/**
 * Constructs an XML label and pickup request.
 *
 * @param $digest
 *   Base-64 encoded shipment request.
 * @param $order_id
 *   The order id of the shipment.
 * @param $packages
 *   An array of package ids to be shipped.
 *
 * @return
 *   ShipmentAcceptRequest XML document to send to Xend.
 */
function uc_xend_request_pickup($digest, $order_id = 0, $packages = array()) {
  $packages = (array)$packages;

  $schema = uc_xend_access_request();

  return $schema;
}